********** All content of ftp://ftp.ncbi.nlm.nih.gov/genbank/genomes/ **********
*********              will be moved on 30-NOV-2015                   **********

Assembled genome sequence and annotation data for GenBank genome assemblies is 
now being provided under:
     ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/
 
On 30 November 2015, or shortly thereafter, all content of this ftp area will be
moved to ftp://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank
The content will not be updated after that date.

________________________________________________________________________________
National Center for Biotechnology Information (NCBI)
National Library of Medicine
National Institutes of Health
8600 Rockville Pike
Bethesda, MD 20894, USA
tel: (301) 496-2475
fax: (301) 480-9241
e-mail: info@ncbi.nlm.nih.gov
________________________________________________________________________________

