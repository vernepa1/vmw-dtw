<?php
// Výpočet max 10 minut
ini_set('max_execution_time', 600);

// Funkce na vytvareni grafu, vystup png graf
function Plotting_Data($data, $a, $t, $c, $g, $names)
{
    require_once("lib/pchart/class/pData.class.php");
    require_once("lib/pchart/class/pDraw.class.php");
    require_once("lib/pchart/class/pImage.class.php");

    $myData = new pData();
    $tics_max = 1;
    $counter = 0;
    foreach ($data as $single_data)
    {
        if (strlen($single_data) > $tics_max)
        {
            $tics_max = strlen($single_data);
        }
        $split_data = str_split($single_data);
        $values = array();
        $suma = 0;
        array_push($values, $suma);
        foreach ($split_data as $single_char)
        {
            switch ($single_char)
            {

                case 'a':
                    $suma += $a;
                    array_push($values, $suma);
                    break;
                case 'c':
                    $suma += $c;
                    array_push($values, $suma);
                    break;
                case 'g':
                    $suma += $g;
                    array_push($values, $suma);
                    break;
                case 't':
                    $suma += $t;
                    array_push($values, $suma);
                    break;

            }
        }
        $myData->addPoints($values, $names[$counter]);
        $counter++;
    }
 $myData->setAxisName(0, "Suma ACGT bází");
 $myData->setAbscissaName("Délka DNA sekvence");
 $myPicture = new pImage(1450,460,$myData);
 $myPicture->setFontProperties(array("FontName"=>"lib/pchart/fonts/Forgotte.ttf","FontSize"=>22));
 $myPicture->setGraphArea(80,40,1350,400);
 if ($tics_max > 20)
 {
    $myPicture->drawScale(array("LabelSkip"=>$tics_max/15));
 }
 else
 {
     $myPicture->drawScale();
 }
 $myPicture->drawSplineChart();
 $myPicture->setFontProperties(array("FontName"=>"lib/pchart/fonts/Forgotte.ttf","FontSize"=>18));
 $myPicture->drawLegend(1360,50,array("Style"=>LEGEND_ROUND,"Mode"=>LEGEND_VERTICAL));
 $path = "plot/plot_" . date("YmdHis") . ".png";
 $myPicture->render($path);
 return $path;
}

// Smazani starych vytvorenych grafu ze slozky plot
$files = glob('plot/*');
foreach($files as $file){
  if(is_file($file))
    unlink($file);
}

// Počáteční hodnoty při prvním spuštění
$raw_string = "";
$baze_a = "0";
$baze_c = "0";
$baze_g = "0";
$baze_t = "0";
$speed = "25";
$pocet_vysl = "10";
$pocet_grafu = "3";

// Dosazení původních hodnot, aby nedocházelo k resetování formuláře
$plots = array();
$error = false;
$comments = '<font color="green">Aplikace doběhla v pořádku!</font>';
$result_picture = 'src="img/result_ok.png" name="mat" value="M" width="30" height="30"';

if (isset($_POST['StringInput']) && $_POST['StringInput'] != '')
{
    $raw_string = $_POST['StringInput'];    
}
else if (isset($_FILES['fileToUpload']) && file_exists($_FILES['fileToUpload']['tmp_name']))
{
}
else 
{
    $error = true;
    $comments = '<font color="red">Zadejte vstupní sekvenci! (přímý vstup / soubor)</font>';   
}
if (isset($_POST['vysl']))
{
    $pocet_vysl = $_POST['vysl'];
    if (!ctype_digit($_POST['vysl']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Počet výsledků musí být celé číslo!</font>';
    }
}
if (isset($_POST['grafu']))
{
    $pocet_grafu = $_POST['grafu'];
    if (!ctype_digit($_POST['grafu']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Počet sekvencí v grafu musí být celé číslo!</font>';
    }
}
if (isset($_POST['speed']))
{
    $speed = $_POST['speed'];
    if (!ctype_digit($_POST['speed']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Oblast výpočtu musí být celé číslo!</font>';
    }
}
if (isset($_POST['A-baze']))
{
    $baze_a = $_POST['A-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['A-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze A musí být celé číslo!</font>";
    }
}
if (isset($_POST['C-baze']))
{
    $baze_c = $_POST['C-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['C-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze C musí být celé číslo!</font>";
    }
}
if (isset($_POST['G-baze']))
{
    $baze_g = $_POST['G-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['G-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze G musí být celé číslo!</font>";
    }
}
if (isset($_POST['T-baze']))
{
    $baze_t = $_POST['T-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['T-baze']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Váha báze T musí být celé číslo!</font>';
    }
}

if ($error)
{
    $result_picture = 'src="img/result_err.png" name="mat" value="M" width="30" height="30"';
}
?>

<html>
    <head>
        <style type="text/css">
        body {
            background: url("img/dna.png") fixed;
            background-position: top right;
            background-repeat: no-repeat;
            background-size: contain;
        }
        img, input {
            inline-block;
            vertical-align: middle;
        }
        </style>
        <meta charset="UTF-8">
        <title>Podobnost DNA</title>
        
        <script>
            function showcase() {
                document.getElementById("StringInput").value = "ATGCCCTATACCTATGATTATCCGCGCCCTGGTCTTACCGTTGACTGCGTGGTGTTTGGCCTAGACGAACAGATCGATCTCAAAGTCCTACTGATTCAGCGCCAGATTCCCCCTTTCCAGCATCAGTGGGCATTACCCGGAGGCTTTGTGCAGATGGATGAATCTTTAGAAGACGCGGCTCGCCGGGAGCTGCGAGAAGAAACGGGGGTTCAGGGTATTTTCCTAGAGCAGCTCTATACCTTTGGCGATTTGGGTCGAGATCCGCGCGATCGCATCATCTCCGTTGCCTACTACGCTCTAATCAACCTCATCGAATATCCTTTACAAGCCTCTACTGATGCTGAAGACGCAGCCTGGTACTCCATTGAGAATTTGCCATCTCTAGCTTTTGATCATGCTCAAATCTTGAAACAGGCCATCCGGCGATTGCAGGGCAAAGTTCGCTACGAACCGATTGGCTTCGAACTACTGCCGCAAAAATTTACACTCACCCAAATTCAGCAGCTTTACGAAACAGTTCTTGGCCATCCTCTAGATAAACGGAACTTTCGTAAGAAATTGCTAAAAATGGATCTCTTAATTCCCCTTGATGAGCAACAAACTGGAGTTGCTCATCGTGCTGCCAGACTCTATCAGTTCGACCAAAGCAAATACGAGCTATTGAAACAACAGGGATTTAACTTCGAGGTTTGA";
                document.getElementById("A-baze").value = "1";
                document.getElementById("C-baze").value = "-2";
                document.getElementById("G-baze").value = "3";
                document.getElementById("T-baze").value = "-4";
                document.getElementById("vysl").value = "10";
                document.getElementById("grafu").value = "3";
                document.getElementById("speed").value = "15";
            }
        </script>
    </head>

    <body>
        <form action="" enctype="multipart/form-data" method='post'>
        <p><h1>Podobnost DNA sekvencí převedených na časové řady</h1></p>
        
        <p><h2>&#x2460; VSTUP</h2></p>
        <button type="button" onclick="showcase()">UKÁZKOVÝ VSTUP</button>
        <p><h3>Přímý vstup</h3></p>
        <textarea class="FormElement" name="StringInput" id="StringInput" style="width: 50%; height: 10em;" placeholder="ACGTACGTACGTACGT..." spellcheck="false"><?php echo $raw_string?></textarea>
        <br>
        <p><h3>Vstup ze souboru</h3></p>
        <input type="file" name="fileToUpload" value="">
        <br><br>
        
        <p><h2>&#x2461; VÁHY</h2></p>
        
        <p><h3>Váhy ACGT bází</h3></p>
        <img src="img/base_a.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="A-baze" id="A-baze" size="2" maxlength="5" value="<?php echo $baze_a?>">
        <br><br>
        <img src="img/base_c.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="C-baze" id="C-baze" size="2" maxlength="5" value="<?php echo $baze_c?>">
        <br><br>
        <img src="img/base_g.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="G-baze" id="G-baze" size="2" maxlength="5" value="<?php echo $baze_g?>">
        <br><br>
        <img src="img/base_t.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="T-baze" id="T-baze" size="2" maxlength="5" value="<?php echo $baze_t?>">
        <br><br>
        
        <p><h2>&#x2462; VÝSTUP</h2></p>
        
        <p><h3>Počet zobrazených výsledků</h3></p>
        <img src="img/results.png" name="mat" value="M" width="76">
        <input type="text" name="vysl" id="vysl" size="2" maxlength="4" value="<?php echo $pocet_vysl?>">
        <br><br>
        
        <p><h3>Počet sekvencí v grafu</h3></p>
        <img src="img/plot.png" name="mat" value="M" width="76">
        <input type="text" name="grafu" id="grafu" size="2" maxlength="2" value="<?php echo $pocet_grafu?>">
        <br><br>
        
        <p><h2>&#x2463; VÝPOČET</h2></p>
        
        <p><h3>Oblast výpočtu</h3></p>
        <img src="img/matrix.png" name="mat" value="M" width="76">
        <input type="text" name="speed" id="speed" size="2" maxlength="3" value="<?php echo $speed?>">
        % <br><p><i>Menší oblast znamená rychlejší výpočet,<br>ale také možnou nepřesnost výsledku.</i></p>
        
        <input type= "submit" name="run" value="SPUSTIT VÝPOČET">
        <br><br>
        
        <?php
        // Tlačítko odeslat bylo zmáčknuto
        if (isset($_POST['run']))
        {
            echo '<img ' . $result_picture . ' style="display: inline-block; vertical-align: middle;">&nbsp;&nbsp;<span style="display: inline-block; vertical-align: middle;"><i>' . $comments . '</i></span><br>';
            
            // Všechny položky byly správně vyplněny
            if (!$error)
            {
                    // Prvně beru přímý vstup
                    if (isset($_POST['StringInput']) && $_POST['StringInput'] != '')
                    {
                        $_POST['StringInput'] = str_replace(' ', '', $_POST['StringInput']);
                        $raw_string = $_POST['StringInput'];
                        exec("cd .. && ./prg " . $_POST['A-baze'] . " " . $_POST['C-baze'] . " " . $_POST['G-baze'] . " " . $_POST['T-baze'] . " " . $_POST['speed'] . " " . $_POST['vysl'] . " " . $_POST['StringInput'], $output, $return_value);
                    }
                    
                    // Pokud není přímý vstup, beru soubor (ten existuje protože !$error)
                    else // if ($_FILES['fileToUpload']['tmp_name'])
                    {
                        $file_content = file_get_contents($_FILES['fileToUpload']['tmp_name']);
                        $file_content = str_replace(' ' , '' , $file_content);
                        exec("cd .. && ./prg " . $_POST['A-baze'] . " " . $_POST['C-baze'] . " " . $_POST['G-baze'] . " " . $_POST['T-baze'] . " " . $_POST['speed'] . " " . $_POST['vysl'] . " " . $file_content, $output, $return_value);
                    }
                    
                    echo '<p><h2>&#x2464; VÝSLEDKY</h2></p>';
                    
                    // Vytvareni dat pro plotovaci funkci
                    $counter = 0;
                    $data_array = array();
                    $names_array = array();
                    // Pocatecni inputova data - original
                    array_push($names_array, "VSTUP");
                    array_push($data_array, strtolower($_POST['StringInput']));
                    if ($pocet_grafu > $pocet_vysl) {$pocet_grafu = $pocet_vysl;}
                    
                    // Parsovani nejblizsich DNA sekvenci do grafu - omezene textboxem pocet grafu
                    foreach ($output as &$value)
                    {
                        // Data pro plotovaci funkci
                        if ($counter < $pocet_grafu)
                        {
                            $raw_data = strtolower(file_get_contents("../test/" . substr($value, strpos($value,':',0) + 1,  strlen($value) - strpos($value,':',0)) . ".seq"));
                            array_push($data_array, $raw_data);
                            array_push($names_array, substr($value, strpos($value,':',0) + 1,  strlen($value) - strpos($value,':',0)) . ".seq");
                            
                            // Zakonceni sberu dat a vztvoreni grafu
                            if ($counter == $pocet_grafu - 1) {
                                echo '<span style="padding-left:0px"></span><img src="' . Plotting_Data($data_array, $_POST['A-baze'], $_POST['T-baze'], $_POST['C-baze'], $_POST['G-baze'], $names_array) . '" name="plot" value="M" style="width: 70%;"><br><br>';
                            }
                        }
                        
                        // Vypisuji jiz pouze zbyle vysledky
                        else
                        {
                            if ($counter == $pocet_vysl) {break;}
                            if ($counter == $pocet_grafu)
                            {
                                // echo '<p><h3>Ostatní výsledky:&nbsp;&nbsp;</h3></p>';
                                echo '<img src="img/result.png" name="mat" value="M" width="50" height="17">';
                                echo '<span style="padding-left:30px">' . $value . '</span><br>';
                            }
                            else
                            {
                                echo '<span style="padding-left:80px">' . $value . '</span><br>';
                            }
                        }
                        
                        $counter++;
                    }
            }
        }
        ?>
        </form>
    </body>
</html>
