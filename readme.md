﻿### Zarovnávání DNA sekvencí převedených na časové řady

DNA sekvenci lze převést na časovou řadu tak, že jednotlivým bázím A,C,G,T přiřadíme váhy (např. A:1, C:2, G:-2, T:-1) a při čtení sekvence upravujeme sumu hodnot podle právě čtené báze. Aplikace by měla umožňovat vizualizaci časových řad vygenerovaných z DNA sekvencí, váhy by měly být nastavitelné (např. ve webovém formuláři) a podobnost časových řad bude řešena pomocí DTW (dynamic time warping) distance.

Edux: [MI-VMW](https://edux.fit.cvut.cz/courses/MI-VMW/tutorials/start#seznam_projektu)

Data: [Genbank](http://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Bacteria/) (soubory typu _ffn_)

Wiki: [DTW](https://en.m.wikipedia.org/wiki/Dynamic_time_warping)
